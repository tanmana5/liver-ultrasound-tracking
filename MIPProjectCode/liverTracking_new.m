%Inputs: 
%       trainingData_path:- Enter the pathname for location of the training
%       data ending in '/'
%       testData_path:- Enter the pathname for location of the test data
%       ending in '/'
%       noOfFrames:Number of test data frames (e.g 50) default=10.
%       CV: Cross validation - 1 cross validation on, generates plots for
%                                tracking information to cross validate against
%                                training data
%                            - 0 cross validation off (default)
%Outputs:
%       M: movie struct object
%       centres=centre positions

%Tuneable parameters:threshold=50 - threshold for distance between segmentation result position and NN computed position
%                    avgIntensity=50 - average intensity of liver structure 


function [centres,M]=liverTracking_new(trainingData_path,testData_path,noOfFrames,CV)

[Med41_x, Med41_y, Med42_x, Med42_y, Med43_x, Med43_y]=liverTracking_data();
% load Med41;
% load Med42;
% load Med43;
if nargin<4
    CV=0;
end

if nargin<3
    noOfFrames=10;
    CV=0;
end

if nargin<2
    fprintf('Please enter the training data path and test data path as arguments');
end

%storing the first image ..............................................
 seed_image = 'liver1.png'
 fname=sprintf(strcat(trainingData_path, seed_image));

I=imread(fname);
 
  
 [centre,properties,compactness]=liverSegmentation(I);
 centres(1,:)=centre;%storing detected centre position at segmentation step


%% Building the Training fv set
[row,col]=size(I);

fv(1,1)=properties(1).Area;
fv(1,2)=properties(1).Eccentricity;
fv(1,3)=properties(1).Orientation;
fv(1,4)=properties(1).EquivDiameter;
fv(1,5)=properties(1).Perimeter;
fv(1,6)=compactness;

target1(1,1)=((Med41_x(1)));
target1(1,2)=((Med41_y(1)));
target2(1,1)=(Med42_x(1));
target2(1,2)=((Med42_y(1)));
target3(1,1)=((Med43_x(1)));
target3(1,1)=((Med43_y(1)));


noOftrngFrames=126;

for frameNum=2:noOftrngFrames
    
 fname=sprintf(strcat(trainingData_path,'liver%d.png'),frameNum);
 I=(imread(fname));
 
 try
[centre,properties,compactness]=liverSegmentation(I);
centres(frameNum,:)=centre;%storing detected centre position at segmentation step
 
fv(frameNum,1)=properties(1).Area;
fv(frameNum,2)=properties(1).Eccentricity;
fv(frameNum,3)=properties(1).Orientation;
fv(frameNum,4)=properties(1).EquivDiameter;
fv(frameNum,5)=properties(1).Perimeter;
fv(frameNum,6)=compactness;


 catch
%      offsetx=(centres(frameNum-1,1)+centres(frameNum-2,1))/2;
%      offsety=(centres(frameNum-1,2)+centres(frameNum-2,2))/2;
%      centre(1,1)=centres(frameNum-1,1)+offsetx;  centre(1,2)=centres(frameNum-1,2)+offsety;
     centres(frameNum,:)=centre;
     fv(frameNum,1)=properties(1).Area;
fv(frameNum,2)=properties(1).Eccentricity;
fv(frameNum,3)=properties(1).Orientation;
fv(frameNum,4)=properties(1).EquivDiameter;
fv(frameNum,5)=properties(1).Perimeter;
fv(frameNum,6)=compactness;
 end
%..........................................................................



%centroid of 1st liver structure....................
target1(frameNum,1)=Med41_x(frameNum);
target1(frameNum,2)=Med41_y(frameNum);
%centroid of 2nd liver structure....................
target2(frameNum,1)=Med42_x(frameNum);
target2(frameNum,2)=Med42_y(frameNum);
%centroid of 3rd liver structure....................
target3(frameNum,1)=Med43_x(frameNum);
target3(frameNum,2)=Med43_y(frameNum);


end 



%% Build the test feature vectors.....................................
% noOfFrames=50; Uncomment to test with 50 test frames
clear centres;

for frameNum=1:noOfFrames
    
 fname=sprintf(strcat(testData_path,'liver%d.png'),frameNum);
 testI=(imread(fname));
 
 try
[centre,properties,compactness]=liverSegmentation(testI);
centres(frameNum,:)=centre;
testfv(frameNum,1)=properties(1).Area;
testfv(frameNum,2)=properties(1).Eccentricity;
testfv(frameNum,3)=properties(1).Orientation;
testfv(frameNum,4)=properties(1).EquivDiameter;
testfv(frameNum,5)=properties(1).Perimeter;
testfv(frameNum,6)=compactness;


 catch
%      offsetx=(centres(frameNum-1,1)+centres(frameNum-2,1))/2;
%      offsety=(centres(frameNum-1,2)+centres(frameNum-2,2))/2;
%      centre(1,1)=centres(frameNum-1,1)+offsetx;  centre(1,2)=centres(frameNum-1,2)+offsety;
centres(frameNum,:)=centre;
testfv(frameNum,1)=properties(1).Area;
testfv(frameNum,2)=properties(1).Eccentricity;
testfv(frameNum,3)=properties(1).Orientation;
testfv(frameNum,4)=properties(1).EquivDiameter;
testfv(frameNum,5)=properties(1).Perimeter;
testfv(frameNum,6)=compactness;
fprintf('false negative in frame number: %d',frameNum);
 end
end
%% neural net based training..........................................
net=patternnet(10);
net1=train(net,double(fv'),double(target1(:,1)'));
out1(:,1)=sim(net1,testfv');

net2=train(net,double(fv'),double(target1(:,2)'));
out1(:,2)=sim(net2,testfv');

net3=train(net,double(fv'),double(target2(:,1)'));
out2(:,1)=sim(net3,testfv');
net4=train(net,double(fv'),double(target2(:,2)'));
out2(:,2)=sim(net4,testfv');

net5=train(net,double(fv'),double(target3(:,1)'));
out3(:,1)=sim(net5,testfv');

net6=train(net,double(fv'),double(target3(:,2)'));
out3(:,2)=sim(net6,testfv');
%% For CROSS VALIDATION ONLY:outcome of only training algorithm...........................................................................
if CV==1
figure,plot(out1(:,1),1:noOfFrames);
title('Neural network tracking in x coordinates 1st strcuture');
 
figure,plot(out1(:,2),1:noOfFrames);
title('Neural network tracking in y coordinates 1st structure');

figure,plot(out2(:,1),1:noOfFrames);
title('Neural network tracking in x coordinates 2nd structure');
 
figure,plot(out2(:,2),1:noOfFrames);
title('Neural network tracking in y coordinates 2nd structure');
figure,plot(out3(:,1),1:noOfFrames);
title('Neural network tracking in x coordinates 3rd structure');
figure,plot(out3(:,2),1:noOfFrames);
title('Neural network tracking in y coordinates 3rd structure');

figure,plot(Med41_x(1:noOfFrames),1:noOfFrames);
title('Actual tracking in x coordinates 1st structure');
figure,plot(Med41_y(1:noOfFrames),1:noOfFrames);
title('Actual tracking in y coordinates 1st structure');
figure,plot(Med42_x(1:noOfFrames),1:noOfFrames);
title('Actual tracking in x coordinates 2nd structure');
figure,plot(Med42_y(1:noOfFrames),1:noOfFrames);
title('Actual tracking in y coordinates 2nd structure');
figure,plot(Med43_x(1:noOfFrames),1:noOfFrames);
title('Actual tracking in x coordinates 3rd structure');
figure,plot(Med43_y(1:noOfFrames),1:noOfFrames);
title('Actual tracking in y coordinates 3rd structure');

%% For CROSS VALIDATION ONLY:tracking the detected centres without training .........................................
  figure,plot(centres(:,1),1:noOfFrames);
  title('tracking without tuning in x coordinates 2nd structure');

        figure,plot(centres(:,2),1:noOfFrames);
         title('tracking without tuning in y coordinates 2nd structure');
end
%% For CROSS VALIDATION ONLY:tuning the tracking with learning results..............................................................

threshold=50; 
for i=1:noOfFrames
    if sqrt((centres(i,1)-out2(i,1)).^2+(centres(i,2)-out2(i ,2)).^2)>threshold
        centres(i,1)=out2(i,1);
        centres(i,2)=out2(i,2);
    end
end

if CV==1
    figure,plot(centres(:,1),1:noOfFrames);
    title('Tuned tracking in x coordinates 2nd structure');

        figure,plot(centres(:,2),1:noOfFrames);
        title('Tuned tracking in y coordinates 2nd structure');
end
%% display positions....


for frameNum=1:noOfFrames
fname=sprintf(strcat(testData_path,'liver%d.png'),frameNum);
testI=(imread(fname));       
figure,imshow(testI)
hold(imgca,'on')
plot(imgca,centres(frameNum,1), centres(frameNum,2), 'r*')
hold(imgca,'off')
 M(frameNum)=getframe(gcf); 
end

%  movie2avi(M,'WaveMovie.avi','fps',2); %Uncomment to create an avi movie