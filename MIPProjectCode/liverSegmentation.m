function [centre,properties,compactness]=liverSegmentation(I,avgIntensity)
if nargin<2
    avgIntensity=50;
end  

% noOfImages=1;
% avgIntensity=50;
% for frameNum=1:noOfImages
%     fname= sprintf('/home/tsadhu/Documents/MATLAB/liver tracking dataset/Training set/MED-04/liverData/liver%d.png',frameNum); 
%  I=(imread(fname));

% diff = anisodiff(I, 5, 30, .1, 1);

BW=double(I)/255< avgIntensity/255;
% entropyI=entropyfilt(I);
% BW=mat2gray(entropyI)<0.8;
% se=strel('disk',10);
% testBW=imclose(openBW,se);
se = strel('disk',12);

% imshow(BW);
% 
openBW = imopen(BW,se);
% figure, imshow(openBW);

[BW1,properties,compactness]=filterRegions(openBW);
% figure,imshow(BW1);

% cc = bwconncomp(openBW);
% stats = regionprops(cc,'Area');
% % figure,imshow(testBW)
% idx = find([stats.Area]<2500);
% [BW1]= ismember(labelmatrix(cc),idx);
% imshow(BW1)

% 
% cc2=bwconncomp(BW1);
% stats2 = regionprops(cc2,'Area');
% idx2 = find([stats2.Area]>1000);
% BW2=ismember(labelmatrix(cc2),idx);
% figure,imshow(BW2);
%% Display the label matrix and draw each boundary
% 
% [B,L] = bwboundaries(BW1,'noholes');
% 
% imshow(label2rgb(L, @jet, [.5 .5 .5]))
% hold on
% for k = 1:length(B)
%   boundary = B{k};
%   plot(boundary(:,2), boundary(:,1), 'w', 'LineWidth', 2)
% end
% figure,imshow(uint8(I));
%% ROI extraction........................................

stats=regionprops(BW1,'centroid');
centre=stats.Centroid;
% patch=I(round(centre(1))-50:round(centre(1))+50,round(centre(2))-50:round(centre(2))+50);
% figure,imshow(I)
% hold(imgca,'on')
% plot(imgca,centre(:,1), centre(:,2), 'r*')
% hold(imgca,'off')


%     patch=I(centre(1)-60:centre(1)+60,centre(2)-60:centre(2)+60);imshow(I)

% figure,imshow(uint8(patch));
end
